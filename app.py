from flask import Flask, render_template, request, jsonify
import joblib
import numpy as np
import os

app = Flask(__name__)

# Obtenez le chemin absolu du répertoire courant
current_directory = os.path.dirname(os.path.abspath(__file__))

# Chargez le modèle en utilisant un chemin absolu
model_path = os.path.join(current_directory, 'iris_model.pkl')
model = joblib.load(model_path)


@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json(force=True)
    # Assurez-vous que les clés 'feature1', 'feature2', 'feature3' sont correctes
    predict_features = np.array(data.get("features"))
 
    # Effectuer une prédiction avec le modèle
    prediction = model.predict([predict_features])
    
    return jsonify({'prediction': int(prediction[0])})


if __name__ == '__main__':
    app.run(port=5000, debug=True)
