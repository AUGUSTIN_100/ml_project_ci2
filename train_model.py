# Import des bibliothèques nécessaires
import pandas as pd
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import joblib

# Chargement du jeu de données (utilisons Iris comme exemple)
iris = datasets.load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# Affichage des données d'entraînement
print("Données d'entraînement:")
print(X_train)

# Création et entraînement du modèle SVM
model = SVC(kernel='linear')
model.fit(X_train, y_train)

# Évaluation du modèle
y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f"Précision du modèle : {accuracy}")

# Sauvegarde du modèle avec joblib
joblib.dump(model, 'iris_model.pkl')
