UML du jeu de rôle

------------------------
Cas d'utilisation:
------------------------
Jouer
Combattre un ennemi
Rencontrer un allié
Rencontrer un objet

-----------------------
Classes:
-----------------------
Personnage
    Attributs:
        Points de vie (PV)
        Valeur d'attaque
        Valeur de défense
        Inventaire (taille maximum 10)
    Méthodes:
        Equiper une arme
        Equiper une armure
        Se soigner avec une potion
        Attaquer un personnage
        Défendre contre une attaque

Joueur
    Hérite de Personnage

Ennemi
    Hérite de Personnage

Allié
    Hérite de Personnage
    Méthode:
        Offrir un objet au joueur

Objet
    Types:
        Potion
        Arme
        Armure
    Attributs:
        Type d'objet
        Effet (pour les potions)
        Bonus d'attaque (pour les armes)
        Bonus de défense (pour les armures)


Relations:

Association:
    Un joueur possède un inventaire.
    Un inventaire contient des objets.
    Un personnage peut équiper une arme.
    Un personnage peut équiper une armure.
    Un joueur peut combattre un ennemi.
    Un joueur peut rencontrer un allié.
    Un joueur peut rencontrer un objet.
    Un allié peut offrir un objet à un joueur.

Hérédité:
    Ennemi et Allié héritent de la classe Personnage.

Surcharge:
    L'opérateur + peut être surchargé pour additionner les points de vie, les valeurs d'attaque et de défense de deux personnages.